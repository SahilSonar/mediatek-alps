LOCAL_PATH := $(call my-dir)

ifeq (,$(wildcard vendor/mediatek/proprietary/packages/apps/LocationEM2))
include $(CLEAR_VARS)
LOCAL_MODULE = LocationEM2
LOCAL_MODULE_CLASS = APPS
LOCAL_MODULE_OWNER = mtk
LOCAL_MODULE_SUFFIX = .apk
LOCAL_PROPRIETARY_MODULE = true
LOCAL_MODULE_TAGS = optional
LOCAL_CERTIFICATE = platform
LOCAL_SRC_FILES = LocationEM2.apk
include $(BUILD_PREBUILT)
endif
