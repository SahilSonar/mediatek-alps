LOCAL_PATH := $(call my-dir)

ifeq (,$(wildcard vendor/mediatek/proprietary/external/met_utils))
include $(CLEAR_VARS)
LOCAL_MODULE = trace-cmd
LOCAL_MODULE_CLASS = EXECUTABLES
LOCAL_MODULE_OWNER = mtk
LOCAL_MULTILIB = 32
LOCAL_SRC_FILES_32 = arm/trace-cmd
include $(BUILD_PREBUILT)
endif
