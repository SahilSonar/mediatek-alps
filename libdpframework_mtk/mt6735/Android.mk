LOCAL_PATH := $(call my-dir)

ifeq (,$(wildcard vendor/mediatek/proprietary/hardware/dpframework))
include $(CLEAR_VARS)
LOCAL_MODULE = libdpframework_mtk
LOCAL_MODULE_CLASS = SHARED_LIBRARIES
LOCAL_MODULE_OWNER = mtk
LOCAL_MODULE_SUFFIX = .so
LOCAL_SHARED_LIBRARIES_64 = libutils libion libcutils liblog libsync libdl libhardware libhidlbase libhidlmemory libion_mtk_sys libgralloc_extra_sys libm4u_sys vendor.mediatek.hardware.pq@2.0 libpq_prot_mtk libnativewindow
LOCAL_EXPORT_C_INCLUDE_DIRS = $(LOCAL_PATH)/include
LOCAL_EXPORT_SHARED_LIBRARY_HEADERS = libnativewindow
LOCAL_MULTILIB = 64
LOCAL_SRC_FILES_64 = arm64/libdpframework_mtk.so
include $(BUILD_PREBUILT)
endif

ifeq (,$(wildcard vendor/mediatek/proprietary/hardware/dpframework))
include $(CLEAR_VARS)
LOCAL_MODULE = libdpframework_mtk
LOCAL_MODULE_CLASS = SHARED_LIBRARIES
LOCAL_MODULE_OWNER = mtk
LOCAL_MODULE_SUFFIX = .so
LOCAL_SHARED_LIBRARIES = libutils libion libcutils liblog libsync libdl libhardware libhidlbase libhidlmemory libion_mtk_sys libgralloc_extra_sys libm4u_sys vendor.mediatek.hardware.pq@2.0 libpq_prot_mtk libnativewindow
LOCAL_EXPORT_C_INCLUDE_DIRS = $(LOCAL_PATH)/include
LOCAL_EXPORT_SHARED_LIBRARY_HEADERS = libnativewindow
LOCAL_MULTILIB = 32
LOCAL_SRC_FILES_32 = arm/libdpframework_mtk.so
include $(BUILD_PREBUILT)
endif
