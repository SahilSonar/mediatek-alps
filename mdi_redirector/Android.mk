LOCAL_PATH := $(call my-dir)

ifeq (,$(wildcard vendor/mediatek/proprietary/frameworks/opt/mdm))
include $(CLEAR_VARS)
LOCAL_MODULE = mdi_redirector
LOCAL_MODULE_CLASS = EXECUTABLES
LOCAL_MODULE_OWNER = mtk
LOCAL_MODULE_TAGS = optional
LOCAL_SHARED_LIBRARIES = libutils liblog libc++ libasn1c_core libasn1c_mapi libapmonitor libbinder
LOCAL_SRC_FILES = $(call get-prebuilt-src-arch,arm arm64)/mdi_redirector
include $(BUILD_PREBUILT)
endif
