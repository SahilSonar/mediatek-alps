LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE = libtouchfilter
LOCAL_MODULE_CLASS = SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX = .so
LOCAL_MODULE_TAGS = optional
LOCAL_SHARED_LIBRARIES_64 = libcutils liblog libutils libinput libc
LOCAL_MULTILIB = 64
LOCAL_SRC_FILES_64 = arm64/libtouchfilter.so
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE = libtouchfilter
LOCAL_MODULE_CLASS = SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX = .so
LOCAL_MODULE_TAGS = optional
LOCAL_SHARED_LIBRARIES = libcutils liblog libutils libinput libc
LOCAL_MULTILIB = 32
LOCAL_SRC_FILES_32 = arm/libtouchfilter.so
include $(BUILD_PREBUILT)
