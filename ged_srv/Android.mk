LOCAL_PATH := $(call my-dir)

ifeq (,$(wildcard vendor/mediatek/proprietary/hardware/gpu_ext))
include $(CLEAR_VARS)
LOCAL_MODULE = ged_srv
LOCAL_MODULE_CLASS = EXECUTABLES
LOCAL_SHARED_LIBRARIES = libbinder libutils liblog libged_sys libui libgui libc libcutils libdl
LOCAL_INIT_RC = ged_srv.rc
LOCAL_SRC_FILES = $(call get-prebuilt-src-arch,arm arm64)/ged_srv
include $(BUILD_PREBUILT)
endif
