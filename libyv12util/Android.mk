LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE = libyv12util
LOCAL_MODULE_CLASS = SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX = .so
LOCAL_MODULE_TAGS = optional
LOCAL_SHARED_LIBRARIES_64 = libnativehelper libcutils libdl liblog libskia libutils libui libandroid libgui libEGL libGLESv2 libandroid_runtime
LOCAL_MULTILIB = 64
LOCAL_SRC_FILES_64 = arm64/libyv12util.so
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE = libyv12util
LOCAL_MODULE_CLASS = SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX = .so
LOCAL_MODULE_TAGS = optional
LOCAL_SHARED_LIBRARIES = libnativehelper libcutils libdl liblog libskia libutils libui libandroid libgui libEGL libGLESv2 libandroid_runtime
LOCAL_MULTILIB = 32
LOCAL_SRC_FILES_32 = arm/libyv12util.so
include $(BUILD_PREBUILT)
