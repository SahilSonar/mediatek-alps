LOCAL_PATH := $(call my-dir)

ifeq (,$(wildcard vendor/mediatek/proprietary/frameworks/opt/mdm))
include $(CLEAR_VARS)
LOCAL_MODULE = apm_jni
LOCAL_MODULE_CLASS = JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX = .jar
LOCAL_UNINSTALLABLE_MODULE = true
LOCAL_MODULE_TAGS = optional
LOCAL_SRC_FILES = javalib.jar
include $(BUILD_PREBUILT)
endif
