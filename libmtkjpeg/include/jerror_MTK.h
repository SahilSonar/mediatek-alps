/*
 * jerror.h
 *
 * Copyright (C) 1994-1997, Thomas G. Lane.
 * This file is part of the Independent JPEG Group's software.
 * For conditions of distribution and use, see the accompanying README file.
 *
 * This file defines the error and message codes for the JPEG library.
 * Edit this file to add new codes, or to translate the message strings to
 * some other language.
 * A set of error-reporting macros are defined too.  Some applications using
 * the JPEG library may wish to include this file to get the error codes
 * and/or the macros.
 */

/*
 * To define the enum list of message codes, include this file without
 * defining macro JMESSAGE.  To create a message string table, include it
 * again with a suitable JMESSAGE definition (see jerror.c for an example).
 */
#ifndef JMESSAGE_MTK
#ifndef JERROR_H_MTK
/* First time through, define the enum list */
#define JMAKE_ENUM_LIST_MTK
#else
/* Repeated inclusions of this file are no-ops unless JMESSAGE is defined */
#define JMESSAGE_MTK(code, string)
#endif				/* JERROR_H */
#endif				/* JMESSAGE */

#ifdef JMAKE_ENUM_LIST_MTK

typedef enum {

#define JMESSAGE_MTK(code, string)	code ,

#endif				/* JMAKE_ENUM_LIST */

	JMESSAGE_MTK(JMSG_NOMESSAGE_MTK, "Bogus message code %d")

	    /* Must be first entry! */
	    /* For maintenance convenience, list is alphabetical by message code name */
	    JMESSAGE_MTK(JERR_ARITH_NOTIMPL_MTK,
		     "Sorry, there are legal restrictions on arithmetic coding")
	    JMESSAGE_MTK(JERR_BAD_ALIGN_TYPE_MTK, "ALIGN_TYPE is wrong, please fix")
	    JMESSAGE_MTK(JERR_BAD_ALLOC_CHUNK_MTK, "MAX_ALLOC_CHUNK is wrong, please fix")
	    JMESSAGE_MTK(JERR_BAD_BUFFER_MODE_MTK, "Bogus buffer control mode")
	    JMESSAGE_MTK(JERR_BAD_COMPONENT_ID_MTK, "Invalid component ID %d in SOS")
	    JMESSAGE_MTK(JERR_BAD_DCT_COEF_MTK, "DCT coefficient out of range")
	    JMESSAGE_MTK(JERR_BAD_DCTSIZE_MTK, "IDCT output block size %d not supported")
	    JMESSAGE_MTK(JERR_BAD_HUFF_TABLE_MTK, "Bogus Huffman table definition")
	    JMESSAGE_MTK(JERR_BAD_IN_COLORSPACE_MTK, "Bogus input colorspace")
	    JMESSAGE_MTK(JERR_BAD_J_COLORSPACE_MTK, "Bogus JPEG colorspace")
	    JMESSAGE_MTK(JERR_BAD_LENGTH_MTK, "Bogus marker length")
	    JMESSAGE_MTK(JERR_BAD_LIB_VERSION_MTK,
		     "Wrong JPEG library version: library is %d, caller expects %d")
	    JMESSAGE_MTK(JERR_BAD_MCU_SIZE_MTK, "Sampling factors too large for interleaved scan")
	    JMESSAGE_MTK(JERR_BAD_POOL_ID_MTK, "Invalid memory pool code %d")
	    JMESSAGE_MTK(JERR_BAD_PRECISION_MTK, "Unsupported JPEG data precision %d")
	    JMESSAGE_MTK(JERR_BAD_PROGRESSION_MTK,
		     "Invalid progressive parameters Ss=%d Se=%d Ah=%d Al=%d")
	    JMESSAGE_MTK(JERR_BAD_PROG_SCRIPT_MTK,
		     "Invalid progressive parameters at scan script entry %d")
	    JMESSAGE_MTK(JERR_BAD_SAMPLING_MTK, "Bogus sampling factors")
	    JMESSAGE_MTK(JERR_BAD_SCAN_SCRIPT_MTK, "Invalid scan script at entry %d")
	    JMESSAGE_MTK(JERR_BAD_STATE_MTK, "Improper call to JPEG library in state %d")
	    JMESSAGE_MTK(JERR_BAD_STRUCT_SIZE_MTK,
		     "JPEG parameter struct mismatch: library thinks size is %u, caller expects %u")
	    JMESSAGE_MTK(JERR_BAD_VIRTUAL_ACCESS_MTK, "Bogus virtual array access")
	    JMESSAGE_MTK(JERR_BUFFER_SIZE_MTK, "Buffer passed to JPEG library is too small")
	    JMESSAGE_MTK(JERR_CANT_SUSPEND_MTK, "Suspension not allowed here")
	    JMESSAGE_MTK(JERR_CCIR601_NOTIMPL_MTK, "CCIR601 sampling not implemented yet")
	    JMESSAGE_MTK(JERR_COMPONENT_COUNT_MTK, "Too many color components: %d, max %d")
	    JMESSAGE_MTK(JERR_CONVERSION_NOTIMPL_MTK, "Unsupported color conversion request")
	    JMESSAGE_MTK(JERR_DAC_INDEX_MTK, "Bogus DAC index %d")
	    JMESSAGE_MTK(JERR_DAC_VALUE_MTK, "Bogus DAC value 0x%x")
	    JMESSAGE_MTK(JERR_DHT_INDEX_MTK, "Bogus DHT index %d")
	    JMESSAGE_MTK(JERR_DQT_INDEX_MTK, "Bogus DQT index %d")
	    JMESSAGE_MTK(JERR_EMPTY_IMAGE_MTK, "Empty JPEG image (DNL not supported)")
	    JMESSAGE_MTK(JERR_EMS_READ_MTK, "Read from EMS failed")
	    JMESSAGE_MTK(JERR_EMS_WRITE_MTK, "Write to EMS failed")
	    JMESSAGE_MTK(JERR_EOI_EXPECTED_MTK, "Didn't expect more than one scan")
	    JMESSAGE_MTK(JERR_FILE_READ_MTK, "Input file read error")
	    JMESSAGE_MTK(JERR_FILE_WRITE_MTK, "Output file write error --- out of disk space?")
	    JMESSAGE_MTK(JERR_FRACT_SAMPLE_NOTIMPL_MTK, "Fractional sampling not implemented yet")
	    JMESSAGE_MTK(JERR_HUFF_CLEN_OVERFLOW_MTK, "Huffman code size table overflow")
	    JMESSAGE_MTK(JERR_HUFF_MISSING_CODE_MTK, "Missing Huffman code table entry")
	    JMESSAGE_MTK(JERR_IMAGE_TOO_BIG_MTK, "Maximum supported image dimension is %u pixels")
	    JMESSAGE_MTK(JERR_INPUT_EMPTY_MTK, "Empty input file")
	    JMESSAGE_MTK(JERR_INPUT_EOF_MTK, "Premature end of input file")
	    JMESSAGE_MTK(JERR_MISMATCHED_QUANT_TABLE_MTK,
		     "Cannot transcode due to multiple use of quantization table %d")
	    JMESSAGE_MTK(JERR_MISSING_DATA_MTK, "Scan script does not transmit all data")
	    JMESSAGE_MTK(JERR_MODE_CHANGE_MTK, "Invalid color quantization mode change")
	    JMESSAGE_MTK(JERR_NOTIMPL_MTK, "Not implemented yet")
	    JMESSAGE_MTK(JERR_NOT_COMPILED_MTK, "Requested feature was omitted at compile time")
	    JMESSAGE_MTK(JERR_NO_BACKING_STORE_MTK, "Backing store not supported")
	    JMESSAGE_MTK(JERR_NO_HUFF_TABLE_MTK, "Huffman table 0x%02x was not defined")
	    JMESSAGE_MTK(JERR_NO_IMAGE_MTK, "JPEG datastream contains no image")
	    JMESSAGE_MTK(JERR_NO_QUANT_TABLE_MTK, "Quantization table 0x%02x was not defined")
	    JMESSAGE_MTK(JERR_NO_SOI_MTK, "Not a JPEG file: starts with 0x%02x 0x%02x")
	    JMESSAGE_MTK(JERR_OUT_OF_MEMORY_MTK, "Insufficient memory (case %d)")
	    JMESSAGE_MTK(JERR_QUANT_COMPONENTS_MTK,
		     "Cannot quantize more than %d color components")
	    JMESSAGE_MTK(JERR_QUANT_FEW_COLORS_MTK, "Cannot quantize to fewer than %d colors")
	    JMESSAGE_MTK(JERR_QUANT_MANY_COLORS_MTK, "Cannot quantize to more than %d colors")
	    JMESSAGE_MTK(JERR_SOF_DUPLICATE_MTK, "Invalid JPEG file structure: two SOF markers")
	    JMESSAGE_MTK(JERR_SOF_NO_SOS_MTK, "Invalid JPEG file structure: missing SOS marker")
	    JMESSAGE_MTK(JERR_SOF_UNSUPPORTED_MTK, "Unsupported JPEG process: SOF type 0x%02x")
	    JMESSAGE_MTK(JERR_SOI_DUPLICATE_MTK, "Invalid JPEG file structure: two SOI markers")
	    JMESSAGE_MTK(JERR_SOS_NO_SOF_MTK, "Invalid JPEG file structure: SOS before SOF")
	    JMESSAGE_MTK(JERR_TFILE_CREATE_MTK, "Failed to create temporary file %s")
	    JMESSAGE_MTK(JERR_TFILE_READ_MTK, "Read failed on temporary file")
	    JMESSAGE_MTK(JERR_TFILE_SEEK_MTK, "Seek failed on temporary file")
	    JMESSAGE_MTK(JERR_TFILE_WRITE_MTK,
		     "Write failed on temporary file --- out of disk space?")
	    JMESSAGE_MTK(JERR_TOO_LITTLE_DATA_MTK, "Application transferred too few scanlines")
	    JMESSAGE_MTK(JERR_UNKNOWN_MARKER_MTK, "Unsupported marker type 0x%02x")
	    JMESSAGE_MTK(JERR_VIRTUAL_BUG_MTK, "Virtual array controller messed up")
	    JMESSAGE_MTK(JERR_WIDTH_OVERFLOW_MTK, "Image too wide for this implementation")
	    JMESSAGE_MTK(JERR_XMS_READ_MTK, "Read from XMS failed")
	    JMESSAGE_MTK(JERR_XMS_WRITE_MTK, "Write to XMS failed")
	    JMESSAGE_MTK(JMSG_COPYRIGHT_MTK, JCOPYRIGHT_MTK)
	    JMESSAGE_MTK(JMSG_VERSION_MTK, JVERSION_MTK)
	    JMESSAGE_MTK(JTRC_16BIT_TABLES_MTK,
		     "Caution: quantization tables are too coarse for baseline JPEG")
	    JMESSAGE_MTK(JTRC_ADOBE_MTK,
		     "Adobe APP14 marker: version %d, flags 0x%04x 0x%04x, transform %d")
	    JMESSAGE_MTK(JTRC_APP0_MTK, "Unknown APP0 marker (not JFIF), length %u")
	    JMESSAGE_MTK(JTRC_APP14_MTK, "Unknown APP14 marker (not Adobe), length %u")
	    JMESSAGE_MTK(JTRC_DAC_MTK, "Define Arithmetic Table 0x%02x: 0x%02x")
	    JMESSAGE_MTK(JTRC_DHT_MTK, "Define Huffman Table 0x%02x")
	    JMESSAGE_MTK(JTRC_DQT_MTK, "Define Quantization Table %d  precision %d")
	    JMESSAGE_MTK(JTRC_DRI_MTK, "Define Restart Interval %u")
	    JMESSAGE_MTK(JTRC_EMS_CLOSE_MTK, "Freed EMS handle %u")
	    JMESSAGE_MTK(JTRC_EMS_OPEN_MTK, "Obtained EMS handle %u")
	    JMESSAGE_MTK(JTRC_EOI_MTK, "End Of Image")
	    JMESSAGE_MTK(JTRC_HUFFBITS_MTK, "        %3d %3d %3d %3d %3d %3d %3d %3d")
	    JMESSAGE_MTK(JTRC_JFIF_MTK, "JFIF APP0 marker: version %d.%02d, density %dx%d  %d")
	    JMESSAGE_MTK(JTRC_JFIF_BADTHUMBNAILSIZE_MTK,
		     "Warning: thumbnail image size does not match data length %u")
	    JMESSAGE_MTK(JTRC_JFIF_EXTENSION_MTK,
		     "JFIF extension marker: type 0x%02x, length %u")
	    JMESSAGE_MTK(JTRC_JFIF_THUMBNAIL_MTK, "    with %d x %d thumbnail image")
	    JMESSAGE_MTK(JTRC_MISC_MARKER_MTK, "Miscellaneous marker 0x%02x, length %u")
	    JMESSAGE_MTK(JTRC_PARMLESS_MARKER_MTK, "Unexpected marker 0x%02x")
	    JMESSAGE_MTK(JTRC_QUANTVALS_MTK, "        %4u %4u %4u %4u %4u %4u %4u %4u")
	    JMESSAGE_MTK(JTRC_QUANT_3_NCOLORS_MTK, "Quantizing to %d = %d*%d*%d colors")
	    JMESSAGE_MTK(JTRC_QUANT_NCOLORS_MTK, "Quantizing to %d colors")
	    JMESSAGE_MTK(JTRC_QUANT_SELECTED_MTK, "Selected %d colors for quantization")
	    JMESSAGE_MTK(JTRC_RECOVERY_ACTION_MTK, "At marker 0x%02x, recovery action %d")
	    JMESSAGE_MTK(JTRC_RST_MTK, "RST%d")
	    JMESSAGE_MTK(JTRC_SMOOTH_NOTIMPL_MTK,
		     "Smoothing not supported with nonstandard sampling ratios")
	    JMESSAGE_MTK(JTRC_SOF_MTK, "Start Of Frame 0x%02x: width=%u, height=%u, components=%d")
	    JMESSAGE_MTK(JTRC_SOF_COMPONENT_MTK, "    Component %d: %dhx%dv q=%d")
	    JMESSAGE_MTK(JTRC_SOI_MTK, "Start of Image")
	    JMESSAGE_MTK(JTRC_SOS_MTK, "Start Of Scan: %d components")
	    JMESSAGE_MTK(JTRC_SOS_COMPONENT_MTK, "    Component %d: dc=%d ac=%d")
	    JMESSAGE_MTK(JTRC_SOS_PARAMS_MTK, "  Ss=%d, Se=%d, Ah=%d, Al=%d")
	    JMESSAGE_MTK(JTRC_TFILE_CLOSE_MTK, "Closed temporary file %s")
	    JMESSAGE_MTK(JTRC_TFILE_OPEN_MTK, "Opened temporary file %s")
	    JMESSAGE_MTK(JTRC_THUMB_JPEG_MTK,
		     "JFIF extension marker: JPEG-compressed thumbnail image, length %u")
	    JMESSAGE_MTK(JTRC_THUMB_PALETTE_MTK,
		     "JFIF extension marker: palette thumbnail image, length %u")
	    JMESSAGE_MTK(JTRC_THUMB_RGB_MTK,
		     "JFIF extension marker: RGB thumbnail image, length %u")
	    JMESSAGE_MTK(JTRC_UNKNOWN_IDS_MTK,
		     "Unrecognized component IDs %d %d %d, assuming YCbCr")
	    JMESSAGE_MTK(JTRC_XMS_CLOSE_MTK, "Freed XMS handle %u")
	    JMESSAGE_MTK(JTRC_XMS_OPEN_MTK, "Obtained XMS handle %u")
	    JMESSAGE_MTK(JWRN_ADOBE_XFORM_MTK, "Unknown Adobe color transform code %d")
	    JMESSAGE_MTK(JWRN_BOGUS_PROGRESSION_MTK,
		     "Inconsistent progression sequence for component %d coefficient %d")
	    JMESSAGE_MTK(JWRN_EXTRANEOUS_DATA_MTK,
		     "Corrupt JPEG data: %u extraneous bytes before marker 0x%02x")
	    JMESSAGE_MTK(JWRN_HIT_MARKER_MTK, "Corrupt JPEG data: premature end of data segment")
	    JMESSAGE_MTK(JWRN_HUFF_BAD_CODE_MTK, "Corrupt JPEG data: bad Huffman code")
	    JMESSAGE_MTK(JWRN_JFIF_MAJOR_MTK, "Warning: unknown JFIF revision number %d.%02d")
	    JMESSAGE_MTK(JWRN_JPEG_EOF_MTK, "Premature end of JPEG file")
	    JMESSAGE_MTK(JWRN_MUST_RESYNC_MTK,
		     "Corrupt JPEG data: found marker 0x%02x instead of RST%d")
	    JMESSAGE_MTK(JWRN_NOT_SEQUENTIAL_MTK, "Invalid SOS parameters for sequential JPEG")
	    JMESSAGE_MTK(JWRN_TOO_MUCH_DATA_MTK, "Application transferred too many scanlines")
#ifdef JMAKE_ENUM_LIST_MTK
	JMSG_LASTMSGCODE_MTK
} J_MESSAGE_CODE_MTK;

#undef JMAKE_ENUM_LIST_MTK
#endif				/* JMAKE_ENUM_LIST */

/* Zap JMESSAGE macro so that future re-inclusions do nothing by default */
#undef JMESSAGE_MTK


#ifndef JERROR_H_MTK
#define JERROR_H_MTK

/* Macros to simplify using the error and trace message stuff */
/* The first parameter is either type of cinfo pointer */

/* Fatal errors (print message and exit) */
#define ERREXIT_MTK(cinfo, code)  \
  ((cinfo)->err->msg_code = (code), \
   (*(cinfo)->err->error_exit) ((j_common_ptr_MTK) (cinfo)))
#define ERREXIT1_MTK(cinfo, code, p1)  \
  ((cinfo)->err->msg_code = (code), \
   (cinfo)->err->msg_parm.i[0] = (p1), \
   (*(cinfo)->err->error_exit) ((j_common_ptr_MTK) (cinfo)))
#define ERREXIT2_MTK(cinfo, code, p1, p2)  \
  ((cinfo)->err->msg_code = (code), \
   (cinfo)->err->msg_parm.i[0] = (p1), \
   (cinfo)->err->msg_parm.i[1] = (p2), \
   (*(cinfo)->err->error_exit) ((j_common_ptr_MTK) (cinfo)))
#define ERREXIT3_MTK(cinfo, code, p1, p2, p3)  \
  ((cinfo)->err->msg_code = (code), \
   (cinfo)->err->msg_parm.i[0] = (p1), \
   (cinfo)->err->msg_parm.i[1] = (p2), \
   (cinfo)->err->msg_parm.i[2] = (p3), \
   (*(cinfo)->err->error_exit) ((j_common_ptr_MTK) (cinfo)))
#define ERREXIT4_MTK(cinfo, code, p1, p2, p3, p4)  \
  ((cinfo)->err->msg_code = (code), \
   (cinfo)->err->msg_parm.i[0] = (p1), \
   (cinfo)->err->msg_parm.i[1] = (p2), \
   (cinfo)->err->msg_parm.i[2] = (p3), \
   (cinfo)->err->msg_parm.i[3] = (p4), \
   (*(cinfo)->err->error_exit) ((j_common_ptr_MTK) (cinfo)))
#define ERREXITS_MTK(cinfo, code, str)  \
  ((cinfo)->err->msg_code = (code), \
   strncpy((cinfo)->err->msg_parm.s, (str), JMSG_STR_PARM_MAX_MTK), \
   (*(cinfo)->err->error_exit) ((j_common_ptr_MTK) (cinfo)))

#define MAKESTMT_MTK(stuff)		do { stuff } while (0)

/* Nonfatal errors (we can keep going, but the data is probably corrupt) */
#define WARNMS_MTK(cinfo, code)  \
  ((cinfo)->err->msg_code = (code), \
   (*(cinfo)->err->emit_message) ((j_common_ptr_MTK) (cinfo), -1))
#define WARNMS1_MTK(cinfo, code, p1)  \
  ((cinfo)->err->msg_code = (code), \
   (cinfo)->err->msg_parm.i[0] = (p1), \
   (*(cinfo)->err->emit_message) ((j_common_ptr_MTK) (cinfo), -1))
#define WARNMS2_MTK(cinfo, code, p1, p2)  \
  ((cinfo)->err->msg_code = (code), \
   (cinfo)->err->msg_parm.i[0] = (p1), \
   (cinfo)->err->msg_parm.i[1] = (p2), \
   (*(cinfo)->err->emit_message) ((j_common_ptr_MTK) (cinfo), -1))

/* Informational/debugging messages */
#define TRACEMS_MTK(cinfo, lvl, code)  \
  ((cinfo)->err->msg_code = (code), \
   (*(cinfo)->err->emit_message) ((j_common_ptr_MTK) (cinfo), (lvl)))
#define TRACEMS1_MTK(cinfo, lvl, code, p1)  \
  ((cinfo)->err->msg_code = (code), \
   (cinfo)->err->msg_parm.i[0] = (p1), \
   (*(cinfo)->err->emit_message) ((j_common_ptr_MTK) (cinfo), (lvl)))
#define TRACEMS2_MTK(cinfo, lvl, code, p1, p2)  \
  ((cinfo)->err->msg_code = (code), \
   (cinfo)->err->msg_parm.i[0] = (p1), \
   (cinfo)->err->msg_parm.i[1] = (p2), \
   (*(cinfo)->err->emit_message) ((j_common_ptr_MTK) (cinfo), (lvl)))
#define TRACEMS3_MTK(cinfo, lvl, code, p1, p2, p3)  \
  MAKESTMT_MTK(int *_mp = (cinfo)->err->msg_parm.i; \
	   _mp[0] = (p1); _mp[1] = (p2); _mp[2] = (p3); \
	   (cinfo)->err->msg_code = (code); \
	   (*(cinfo)->err->emit_message) ((j_common_ptr_MTK) (cinfo), (lvl));)
#define TRACEMS4_MTK(cinfo, lvl, code, p1, p2, p3, p4)  \
  MAKESTMT_MTK(int *_mp = (cinfo)->err->msg_parm.i; \
	   _mp[0] = (p1); _mp[1] = (p2); _mp[2] = (p3); _mp[3] = (p4); \
	   (cinfo)->err->msg_code = (code); \
	   (*(cinfo)->err->emit_message) ((j_common_ptr_MTK) (cinfo), (lvl));)
#define TRACEMS5_MTK(cinfo, lvl, code, p1, p2, p3, p4, p5)  \
  MAKESTMT_MTK(int *_mp = (cinfo)->err->msg_parm.i; \
	   _mp[0] = (p1); _mp[1] = (p2); _mp[2] = (p3); _mp[3] = (p4); \
	   _mp[4] = (p5); \
	   (cinfo)->err->msg_code = (code); \
	   (*(cinfo)->err->emit_message) ((j_common_ptr_MTK) (cinfo), (lvl));)
#define TRACEMS8_MTK(cinfo, lvl, code, p1, p2, p3, p4, p5, p6, p7, p8)  \
  MAKESTMT_MTK(int *_mp = (cinfo)->err->msg_parm.i; \
	   _mp[0] = (p1); _mp[1] = (p2); _mp[2] = (p3); _mp[3] = (p4); \
	   _mp[4] = (p5); _mp[5] = (p6); _mp[6] = (p7); _mp[7] = (p8); \
	   (cinfo)->err->msg_code = (code); \
	   (*(cinfo)->err->emit_message) ((j_common_ptr_MTK) (cinfo), (lvl));)
#define TRACEMSS_MTK(cinfo, lvl, code, str)  \
  ((cinfo)->err->msg_code = (code), \
   strncpy((cinfo)->err->msg_parm.s, (str), JMSG_STR_PARM_MAX_MTK), \
   (*(cinfo)->err->emit_message) ((j_common_ptr_MTK) (cinfo), (lvl)))

#endif				/* JERROR_H */
