/*
 * jchuff.h
 *
 * Copyright (C) 1991-1997, Thomas G. Lane.
 * This file is part of the Independent JPEG Group's software.
 * For conditions of distribution and use, see the accompanying README file.
 *
 * This file contains declarations for Huffman entropy encoding routines
 * that are shared between the sequential encoder (jchuff.c) and the
 * progressive encoder (jcphuff.c).  No other modules need to see these.
 */

/* The legal range of a DCT coefficient is
 *  -1024 .. +1023  for 8-bit data;
 * -16384 .. +16383 for 12-bit data.
 * Hence the magnitude should always fit in 10 or 14 bits respectively.
 */

#if BITS_IN_JSAMPLE_MTK == 8
#define MAX_COEF_BITS 10
#else
#define MAX_COEF_BITS 14
#endif

/* Derived data constructed for each Huffman table */

typedef struct {
	unsigned int ehufco[256];	/* code for each symbol */
	char ehufsi[256];	/* length of code for each symbol */
	/* If no code has been allocated for a symbol S, ehufsi[S] contains 0 */
} c_derived_tbl_MTK;

/* Short forms of external names for systems with brain-damaged linkers. */

#ifdef NEED_SHORT_EXTERNAL_NAMES_MTK
#define jpeg_make_c_derived_tbl_MTK	jMkCDerived
#define jpeg_gen_optimal_table_MTK	jGenOptTbl
#endif				/* NEED_SHORT_EXTERNAL_NAMES */

/* Expand a Huffman table definition into the derived format */
EXTERN_MTK(void) jpeg_make_c_derived_tbl_MTK
JPP_MTK((j_compress_ptr_MTK cinfo, boolean_MTK isDC, int tblno, c_derived_tbl_MTK * *pdtbl));

/* Generate an optimal table definition given the specified counts */
EXTERN_MTK(void) jpeg_gen_optimal_table_MTK JPP_MTK((j_compress_ptr_MTK cinfo, JHUFF_TBL_MTK * htbl, long freq[]));



