/*
 * cderror.h
 *
 * Copyright (C) 1994-1997, Thomas G. Lane.
 * This file is part of the Independent JPEG Group's software.
 * For conditions of distribution and use, see the accompanying README file.
 *
 * This file defines the error and message codes for the cjpeg/djpeg
 * applications.  These strings are not needed as part of the JPEG library
 * proper.
 * Edit this file to add new codes, or to translate the message strings to
 * some other language.
 */

/*
 * To define the enum list of message codes, include this file without
 * defining macro JMESSAGE.  To create a message string table, include it
 * again with a suitable JMESSAGE definition (see jerror.c for an example).
 */
#ifndef JMESSAGE_MTK
#ifndef CDERROR_H_MTK
#define CDERROR_H_MTK
/* First time through, define the enum list */
#define JMAKE_ENUM_LIST_MTK
#else
/* Repeated inclusions of this file are no-ops unless JMESSAGE is defined */
#define JMESSAGE_MTK(code, string)
#endif				/* CDERROR_H */
#endif				/* JMESSAGE */

#ifdef JMAKE_ENUM_LIST_MTK

typedef enum {

#define JMESSAGE_MTK(code, string)	code ,

#endif				/* JMAKE_ENUM_LIST */

	JMESSAGE_MTK(JMSG_FIRSTADDONCODE = 1000, NULL)
	    /* Must be first entry! */
#ifdef BMP_SUPPORTED_MTK
	    JMESSAGE_MTK(JERR_BMP_BADCMAP, "Unsupported BMP colormap format")
	    JMESSAGE_MTK(JERR_BMP_BADDEPTH, "Only 8- and 24-bit BMP files are supported")
	    JMESSAGE_MTK(JERR_BMP_BADHEADER, "Invalid BMP file: bad header length")
	    JMESSAGE_MTK(JERR_BMP_BADPLANES, "Invalid BMP file: biPlanes not equal to 1")
	    JMESSAGE_MTK(JERR_BMP_COLORSPACE, "BMP output must be grayscale or RGB")
	    JMESSAGE_MTK(JERR_BMP_COMPRESSED, "Sorry, compressed BMPs not yet supported")
	    JMESSAGE_MTK(JERR_BMP_NOT, "Not a BMP file - does not start with BM")
	    JMESSAGE_MTK(JTRC_BMP, "%ux%u 24-bit BMP image")
	    JMESSAGE_MTK(JTRC_BMP_MAPPED, "%ux%u 8-bit colormapped BMP image")
	    JMESSAGE_MTK(JTRC_BMP_OS2, "%ux%u 24-bit OS2 BMP image")
	    JMESSAGE_MTK(JTRC_BMP_OS2_MAPPED, "%ux%u 8-bit colormapped OS2 BMP image")
#endif				/* BMP_SUPPORTED */
#ifdef GIF_SUPPORTED_MTK
	    JMESSAGE_MTK(JERR_GIF_BUG, "GIF output got confused")
	    JMESSAGE_MTK(JERR_GIF_CODESIZE, "Bogus GIF codesize %d")
	    JMESSAGE_MTK(JERR_GIF_COLORSPACE, "GIF output must be grayscale or RGB")
	    JMESSAGE_MTK(JERR_GIF_IMAGENOTFOUND, "Too few images in GIF file")
	    JMESSAGE_MTK(JERR_GIF_NOT, "Not a GIF file")
	    JMESSAGE_MTK(JTRC_GIF, "%ux%ux%d GIF image")
	    JMESSAGE_MTK(JTRC_GIF_BADVERSION,
		     "Warning: unexpected GIF version number '%c%c%c'")
	    JMESSAGE_MTK(JTRC_GIF_EXTENSION, "Ignoring GIF extension block of type 0x%02x")
	    JMESSAGE_MTK(JTRC_GIF_NONSQUARE, "Caution: nonsquare pixels in input")
	    JMESSAGE_MTK(JWRN_GIF_BADDATA, "Corrupt data in GIF file")
	    JMESSAGE_MTK(JWRN_GIF_CHAR, "Bogus char 0x%02x in GIF file, ignoring")
	    JMESSAGE_MTK(JWRN_GIF_ENDCODE, "Premature end of GIF image")
	    JMESSAGE_MTK(JWRN_GIF_NOMOREDATA, "Ran out of GIF bits")
#endif				/* GIF_SUPPORTED */
#ifdef PPM_SUPPORTED_MTK
	    JMESSAGE_MTK(JERR_PPM_COLORSPACE, "PPM output must be grayscale or RGB")
	    JMESSAGE_MTK(JERR_PPM_NONNUMERIC, "Nonnumeric data in PPM file")
	    JMESSAGE_MTK(JERR_PPM_NOT, "Not a PPM/PGM file")
	    JMESSAGE_MTK(JTRC_PGM, "%ux%u PGM image")
	    JMESSAGE_MTK(JTRC_PGM_TEXT, "%ux%u text PGM image")
	    JMESSAGE_MTK(JTRC_PPM, "%ux%u PPM image")
	    JMESSAGE_MTK(JTRC_PPM_TEXT, "%ux%u text PPM image")
#endif				/* PPM_SUPPORTED */
#ifdef RLE_SUPPORTED_MTK
	    JMESSAGE_MTK(JERR_RLE_BADERROR, "Bogus error code from RLE library")
	    JMESSAGE_MTK(JERR_RLE_COLORSPACE, "RLE output must be grayscale or RGB")
	    JMESSAGE_MTK(JERR_RLE_DIMENSIONS, "Image dimensions (%ux%u) too large for RLE")
	    JMESSAGE_MTK(JERR_RLE_EMPTY, "Empty RLE file")
	    JMESSAGE_MTK(JERR_RLE_EOF, "Premature EOF in RLE header")
	    JMESSAGE_MTK(JERR_RLE_MEM, "Insufficient memory for RLE header")
	    JMESSAGE_MTK(JERR_RLE_NOT, "Not an RLE file")
	    JMESSAGE_MTK(JERR_RLE_TOOMANYCHANNELS, "Cannot handle %d output channels for RLE")
	    JMESSAGE_MTK(JERR_RLE_UNSUPPORTED, "Cannot handle this RLE setup")
	    JMESSAGE_MTK(JTRC_RLE, "%ux%u full-color RLE file")
	    JMESSAGE_MTK(JTRC_RLE_FULLMAP, "%ux%u full-color RLE file with map of length %d")
	    JMESSAGE_MTK(JTRC_RLE_GRAY, "%ux%u grayscale RLE file")
	    JMESSAGE_MTK(JTRC_RLE_MAPGRAY, "%ux%u grayscale RLE file with map of length %d")
	    JMESSAGE_MTK(JTRC_RLE_MAPPED, "%ux%u colormapped RLE file with map of length %d")
#endif				/* RLE_SUPPORTED */
#ifdef TARGA_SUPPORTED_MTK
	    JMESSAGE_MTK(JERR_TGA_BADCMAP, "Unsupported Targa colormap format")
	    JMESSAGE_MTK(JERR_TGA_BADPARMS, "Invalid or unsupported Targa file")
	    JMESSAGE_MTK(JERR_TGA_COLORSPACE, "Targa output must be grayscale or RGB")
	    JMESSAGE_MTK(JTRC_TGA, "%ux%u RGB Targa image")
	    JMESSAGE_MTK(JTRC_TGA_GRAY, "%ux%u grayscale Targa image")
	    JMESSAGE_MTK(JTRC_TGA_MAPPED, "%ux%u colormapped Targa image")
#else
	    JMESSAGE_MTK(JERR_TGA_NOTCOMP, "Targa support was not compiled")
#endif				/* TARGA_SUPPORTED */
	    JMESSAGE_MTK(JERR_BAD_CMAP_FILE,
		     "Color map file is invalid or of unsupported format")
	    JMESSAGE_MTK(JERR_TOO_MANY_COLORS,
		     "Output file format cannot handle %d colormap entries")
	    JMESSAGE_MTK(JERR_UNGETC_FAILED, "ungetc failed")
#ifdef TARGA_SUPPORTED_MTK
	    JMESSAGE_MTK(JERR_UNKNOWN_FORMAT,
		     "Unrecognized input file format --- perhaps you need -targa")
#else
	    JMESSAGE_MTK(JERR_UNKNOWN_FORMAT, "Unrecognized input file format")
#endif
	    JMESSAGE_MTK(JERR_UNSUPPORTED_FORMAT, "Unsupported output file format")
#ifdef JMAKE_ENUM_LIST_MTK
	JMSG_LASTADDONCODE
} ADDON_MESSAGE_CODE_MTK;

#undef JMAKE_ENUM_LIST_MTK
#endif				/* JMAKE_ENUM_LIST */

/* Zap JMESSAGE macro so that future re-inclusions do nothing by default */
#undef JMESSAGE_MTK
