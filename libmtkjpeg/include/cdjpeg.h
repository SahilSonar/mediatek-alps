/*
 * cdjpeg.h
 *
 * Copyright (C) 1994-1997, Thomas G. Lane.
 * This file is part of the Independent JPEG Group's software.
 * For conditions of distribution and use, see the accompanying README file.
 *
 * This file contains common declarations for the sample applications
 * cjpeg and djpeg.  It is NOT used by the core JPEG library.
 */

#define JPEG_CJPEG_DJPEG_MTK	/* define proper options in jconfig.h */
#define JPEG_INTERNAL_OPTIONS_MTK	/* cjpeg.c,djpeg.c need to see xxx_SUPPORTED */
#include "jinclude.h"
#include "jpeglib.h"
#include "jerror.h"		/* get library error codes too */
#include "cderror.h"		/* get application-specific error codes */


/*
 * Object interface for cjpeg's source file decoding modules
 */

typedef struct cjpeg_source_struct *cjpeg_source_ptr;

struct cjpeg_source_struct {
	JMETHOD_MTK(void, start_input, (j_compress_ptr_MTK cinfo, cjpeg_source_ptr sinfo));
	 JMETHOD_MTK(JDIMENSION_MTK, get_pixel_rows, (j_compress_ptr_MTK cinfo, cjpeg_source_ptr sinfo));
	 JMETHOD_MTK(void, finish_input, (j_compress_ptr_MTK cinfo, cjpeg_source_ptr sinfo));

	FILE *input_file;

	JSAMPARRAY_MTK buffer;
	JDIMENSION_MTK buffer_height;
};


/*
 * Object interface for djpeg's output file encoding modules
 */

typedef struct djpeg_dest_struct *djpeg_dest_ptr;

struct djpeg_dest_struct {
	/* start_output is called after jpeg_start_decompress finishes.
	 * The color map will be ready at this time, if one is needed.
	 */
	JMETHOD_MTK(void, start_output, (j_decompress_ptr_MTK cinfo, djpeg_dest_ptr dinfo));
	/* Emit the specified number of pixel rows from the buffer. */
	 JMETHOD_MTK(void, put_pixel_rows, (j_decompress_ptr_MTK cinfo,
					djpeg_dest_ptr dinfo, JDIMENSION_MTK rows_supplied));
	/* Finish up at the end of the image. */
	 JMETHOD_MTK(void, finish_output, (j_decompress_ptr_MTK cinfo, djpeg_dest_ptr dinfo));

	/* Target file spec; filled in by djpeg.c after object is created. */
	FILE *output_file;

	/* Output pixel-row buffer.  Created by module init or start_output.
	 * Width is cinfo->output_width * cinfo->output_components;
	 * height is buffer_height.
	 */
	JSAMPARRAY_MTK buffer;
	JDIMENSION_MTK buffer_height;
};


/*
 * cjpeg/djpeg may need to perform extra passes to convert to or from
 * the source/destination file format.  The JPEG library does not know
 * about these passes, but we'd like them to be counted by the progress
 * monitor.  We use an expanded progress monitor object to hold the
 * additional pass count.
 */

struct cdjpeg_progress_mgr {
	struct jpeg_progress_mgr_MTK pub;	/* fields known to JPEG library */
	int completed_extra_passes;	/* extra passes completed */
	int total_extra_passes;	/* total extra */
	/* last printed percentage stored here to avoid multiple printouts */
	int percent_done;
};

typedef struct cdjpeg_progress_mgr *cd_progress_ptr;


/* Short forms of external names for systems with brain-damaged linkers. */

#ifdef NEED_SHORT_EXTERNAL_NAMES_MTK
#define jinit_read_bmp		jIRdBMP
#define jinit_write_bmp		jIWrBMP
#define jinit_read_gif		jIRdGIF
#define jinit_write_gif		jIWrGIF
#define jinit_read_ppm		jIRdPPM
#define jinit_write_ppm		jIWrPPM
#define jinit_read_rle		jIRdRLE
#define jinit_write_rle		jIWrRLE
#define jinit_read_targa	jIRdTarga
#define jinit_write_targa	jIWrTarga
#define read_quant_tables	RdQTables
#define read_scan_script	RdScnScript
#define set_quant_slots		SetQSlots
#define set_sample_factors	SetSFacts
#define read_color_map		RdCMap
#define enable_signal_catcher	EnSigCatcher
#define start_progress_monitor	StProgMon
#define end_progress_monitor	EnProgMon
#define read_stdin		RdStdin
#define write_stdout		WrStdout
#endif				/* NEED_SHORT_EXTERNAL_NAMES */

/* Module selection routines for I/O modules. */

EXTERN_MTK(cjpeg_source_ptr)
jinit_read_bmp JPP_MTK((j_compress_ptr_MTK cinfo));
EXTERN_MTK(djpeg_dest_ptr)
jinit_write_bmp JPP_MTK((j_decompress_ptr_MTK cinfo, boolean_MTK is_os2));
EXTERN_MTK(cjpeg_source_ptr)
jinit_read_gif JPP_MTK((j_compress_ptr_MTK cinfo));
EXTERN_MTK(djpeg_dest_ptr)
jinit_write_gif JPP_MTK((j_decompress_ptr_MTK cinfo));
EXTERN_MTK(cjpeg_source_ptr)
jinit_read_ppm JPP_MTK((j_compress_ptr_MTK cinfo));
EXTERN_MTK(djpeg_dest_ptr)
jinit_write_ppm JPP_MTK((j_decompress_ptr_MTK cinfo));
EXTERN_MTK(cjpeg_source_ptr)
jinit_read_rle JPP_MTK((j_compress_ptr_MTK cinfo));
EXTERN_MTK(djpeg_dest_ptr)
jinit_write_rle JPP_MTK((j_decompress_ptr_MTK cinfo));
EXTERN_MTK(cjpeg_source_ptr)
jinit_read_targa JPP_MTK((j_compress_ptr_MTK cinfo));
EXTERN_MTK(djpeg_dest_ptr)
jinit_write_targa JPP_MTK((j_decompress_ptr_MTK cinfo));

/* cjpeg support routines (in rdswitch.c) */

EXTERN_MTK(boolean_MTK)
read_quant_tables JPP_MTK((j_compress_ptr_MTK cinfo, char *filename,
		       int scale_factor, boolean_MTK force_baseline));
EXTERN_MTK(boolean_MTK)
read_scan_script JPP_MTK((j_compress_ptr_MTK cinfo, char *filename));
EXTERN_MTK(boolean_MTK)
set_quant_slots JPP_MTK((j_compress_ptr_MTK cinfo, char *arg));
EXTERN_MTK(boolean_MTK)
set_sample_factors JPP_MTK((j_compress_ptr_MTK cinfo, char *arg));

/* djpeg support routines (in rdcolmap.c) */

EXTERN_MTK(void)
read_color_map JPP_MTK((j_decompress_ptr_MTK cinfo, FILE *infile));

/* common support routines (in cdjpeg.c) */

EXTERN_MTK(void)
enable_signal_catcher JPP_MTK((j_common_ptr_MTK cinfo));
EXTERN_MTK(void)
start_progress_monitor JPP_MTK((j_common_ptr_MTK cinfo, cd_progress_ptr progress));
EXTERN_MTK(void)
end_progress_monitor JPP_MTK((j_common_ptr_MTK cinfo));
EXTERN_MTK(boolean_MTK)
keymatch JPP_MTK((char *arg, const char *keyword, int minchars));
EXTERN_MTK(FILE *)
read_stdin JPP_MTK((void));
EXTERN_MTK(FILE *)
write_stdout JPP_MTK((void));

/* miscellaneous useful macros */

#ifdef DONT_USE_B_MODE_MTK		/* define mode parameters for fopen() */
#define READ_BINARY	"r"
#define WRITE_BINARY	"w"
#else
#ifdef VMS			/* VMS is very nonstandard */
#define READ_BINARY	"rb", "ctx=stm"
#define WRITE_BINARY	"wb", "ctx=stm"
#else				/* standard ANSI-compliant case */
#define READ_BINARY	"rb"
#define WRITE_BINARY	"wb"
#endif
#endif

#ifndef EXIT_FAILURE		/* define exit() codes if not provided */
#define EXIT_FAILURE  1
#endif
#ifndef EXIT_SUCCESS
#ifdef VMS
#define EXIT_SUCCESS  1		/* VMS is very nonstandard */
#else
#define EXIT_SUCCESS  0
#endif
#endif
#ifndef EXIT_WARNING
#ifdef VMS
#define EXIT_WARNING  1		/* VMS is very nonstandard */
#else
#define EXIT_WARNING  2
#endif
#endif
