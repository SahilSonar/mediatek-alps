LOCAL_PATH := $(call my-dir)

ifeq (,$(wildcard vendor/mediatek/proprietary/external/met_utils))
include $(CLEAR_VARS)
LOCAL_MODULE = met_log_d
LOCAL_MODULE_CLASS = EXECUTABLES
LOCAL_MODULE_OWNER = mtk
LOCAL_SHARED_LIBRARIES = liblog libcutils
LOCAL_INIT_RC = ./met_log_d.rc
LOCAL_MULTILIB = 32
LOCAL_SRC_FILES_32 = arm/met_log_d
include $(BUILD_PREBUILT)
endif
