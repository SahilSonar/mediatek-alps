LOCAL_PATH := $(call my-dir)

ifeq (,$(wildcard vendor/mediatek/proprietary/hardware/libomx/audio))
include $(CLEAR_VARS)
LOCAL_MODULE = libMtkOmxApeDec
LOCAL_MODULE_CLASS = SHARED_LIBRARIES
LOCAL_MODULE_OWNER = mtk
LOCAL_MODULE_SUFFIX = .so
LOCAL_PROPRIETARY_MODULE = true
LOCAL_SHARED_LIBRARIES = libutils libcutils libdl libui liblog libhidlbase libhidltransport libhwbinder libhidlmemory vendor.mediatek.hardware.mtkcodecservice@1.1_vendor android.hidl.allocator@1.0 android.hidl.memory@1.0
LOCAL_MULTILIB = 32
LOCAL_SRC_FILES_32 = arm/libMtkOmxApeDec.so
include $(BUILD_PREBUILT)
endif
