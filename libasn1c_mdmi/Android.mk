LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE = libasn1c_mdmi
LOCAL_MODULE_CLASS = SHARED_LIBRARIES
LOCAL_MODULE_OWNER = mtk
LOCAL_MODULE_SUFFIX = .so
LOCAL_MODULE_TAGS = optional
LOCAL_SHARED_LIBRARIES_64 = libcutils libasn1c_core
LOCAL_MULTILIB = 64
LOCAL_SRC_FILES_64 = arm64/libasn1c_mdmi.so
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE = libasn1c_mdmi
LOCAL_MODULE_CLASS = SHARED_LIBRARIES
LOCAL_MODULE_OWNER = mtk
LOCAL_MODULE_SUFFIX = .so
LOCAL_MODULE_TAGS = optional
LOCAL_SHARED_LIBRARIES = libcutils libasn1c_core
LOCAL_MULTILIB = 32
LOCAL_SRC_FILES_32 = arm/libasn1c_mdmi.so
include $(BUILD_PREBUILT)
