LOCAL_PATH := $(call my-dir)

ifeq (,$(wildcard vendor/mediatek/proprietary/hardware/libvcodec))
include $(CLEAR_VARS)
LOCAL_MODULE = mfv_ut
LOCAL_MODULE_CLASS = EXECUTABLES
LOCAL_MODULE_OWNER = mtk
LOCAL_PROPRIETARY_MODULE = true
LOCAL_MODULE_TAGS = optional
LOCAL_MODULE_STEM_32 = mfv_ut
LOCAL_SHARED_LIBRARIES = libcutils libvcodecdrv libvcodec_utility liblog libmtk_drvb libdl
LOCAL_MULTILIB = 32
LOCAL_SRC_FILES_32 = arm/mfv_ut
include $(BUILD_PREBUILT)
endif
