LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE = libvoicerecognition_jni
LOCAL_MODULE_CLASS = SHARED_LIBRARIES
LOCAL_MODULE_OWNER = mtk
LOCAL_MODULE_SUFFIX = .so
LOCAL_PROPRIETARY_MODULE = false
LOCAL_MODULE_TAGS = optional
LOCAL_SHARED_LIBRARIES_64 = libui libutils libbinder libandroid_runtime libnativehelper libcutils libmedia libvoicerecognition
LOCAL_MULTILIB = 64
LOCAL_SRC_FILES_64 = arm64/libvoicerecognition_jni.so
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE = libvoicerecognition_jni
LOCAL_MODULE_CLASS = SHARED_LIBRARIES
LOCAL_MODULE_OWNER = mtk
LOCAL_MODULE_SUFFIX = .so
LOCAL_PROPRIETARY_MODULE = false
LOCAL_MODULE_TAGS = optional
LOCAL_SHARED_LIBRARIES = libui libutils libbinder libandroid_runtime libnativehelper libcutils libmedia libvoicerecognition
LOCAL_MULTILIB = 32
LOCAL_SRC_FILES_32 = arm/libvoicerecognition_jni.so
include $(BUILD_PREBUILT)
